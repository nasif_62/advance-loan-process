from odoo import fields, models, api
from odoo.tools.misc import formatLang, format_date
from odoo.exceptions import ValidationError, UserError, RedirectWarning


class ChangeEMIAmount(models.TransientModel):
    _name = "change.emi.amount"
    _description = "Change EMI Amount"

    emi_date = fields.Date(string="EMI Date")
    new_emi_amount = fields.Float(string="Changed EMI Amount")

    def change_emi_amount(self):
        record = self.env['loan.request'].browse([(self._context['active_id'])])
        str_date = format_date(self.env, fields.Date.to_string(self.emi_date), date_format='MMMM yyyy')

        validate = 0
        for rec in record.emi_detail_line:
            if rec.str_date_emi == str_date and rec.skip_month == False and rec.loan_status == False:
                validate = 1
                break # use self.update

        if validate == 1:
            active_line = len(list(filter(lambda x: x.skip_month == False and x.loan_status == False, record.emi_detail_line)))
            updated_emi_amount = (record.emi_detail_line.loan_request_id.remaining_loan_amount - self.new_emi_amount) / (active_line - 1)
            if updated_emi_amount > 0:
                for line in record.emi_detail_line:
                    if line.str_date_emi == str_date:
                        line.update({'emi_amount': self.new_emi_amount})
                    elif line.skip_month == False and line.loan_status == False:
                        line.update({'emi_amount': updated_emi_amount})
            else:
                raise ValidationError("Please recheck your changed EMI Amount!")
        else:
            raise ValidationError("Wrong Month Selected!")
