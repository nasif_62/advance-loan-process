{
    'name': "Advance Loan Process",
    'version': '1.0',
    'depends': ['hr', 'hr_payroll'],
    'author': "BS-23",
    'category': 'Category',
    'description': """
    Description text
    """,
    'data': [
        'security/loan_security.xml',
        'security/ir.model.access.csv',
        'views/loan_policy.xml',
        'views/menus.xml',
        'wizards/change_emi_amount.xml',
        'views/loan_request.xml',
    ],
    'application': True,
    'license': 'LGPL-3',
}
