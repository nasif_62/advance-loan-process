
from odoo import fields, models
from bs_lib.service import app_constant


class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    _description = 'Employee'

    employee_type_selection = [
        ('regular', 'Regular'),
        ('probationer', 'Probationer'),
        ('contractual', 'Contractual'),
        ('trainee', 'Trainee'),
        ('advisor', 'Advisor')
    ]

    birth_certificate_number = fields.Char("Birth Certificate Number")
    blood_group = fields.Selection(selection=app_constant.blood_group_selection, string="Blood Group")
    employee_types = fields.Selection(selection=employee_type_selection, string="Employee Type")

    # Calculate loan EMI deduction for employee from (payroll > loan)
    # Using data from salary rule's custom python code
    # payslip parameter can not be NULL, USE: employee.get_loan_emi_amount(payslip) in Salary-rule python code
    def get_loan_emi_amount(self, payslip):
        emi_lines = self.env['loan.emi.details'].sudo()
        for employee in self:
            employee_loan_inst_lines = emi_lines.search([('loan_request_id.employee_id', '=', employee.id),
                                                                ('date_emi', '=', payslip.date_from),
                                                                ('skip_month', '=', False)])
            total_loan = 0.0
            for line in employee_loan_inst_lines:
                total_loan += line.emi_amount
            return total_loan * -1
