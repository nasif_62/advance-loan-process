# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, SUPERUSER_ID
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from odoo.tools.misc import formatLang, format_date
from dateutil.relativedelta import relativedelta


class LoanRequest(models.Model):
    _name = 'loan.request'

    state_selection = [
        ('draft', 'Draft'),
        ('apply', 'Applied'),
        ('confirm', 'In-Progress'),
        ('done', 'Closed'),
        ('cancel', 'Cancelled')
    ]

    def _get_default_loan_policy(self):
        active_loan_policy = self.env['loan.policy'].sudo().search([
            ('state', '=', 'confirm')
        ], limit=1)
        return active_loan_policy

    def _get_default_employee(self):
        employee = self.env['hr.employee'].sudo().search([
            ('user_id', '=', self.env.uid)
        ], limit=1)
        return employee

    employee_id = fields.Many2one("hr.employee", string="Project Manager",
                                  default=_get_default_employee)
    loan_policy = fields.Many2one(comodel_name="loan.policy", required=True, string="Loan Policy", default=_get_default_loan_policy)

    job_id = fields.Char(string="Job ID", related="employee_id.barcode", readonly=True)
    department_name = fields.Char(string="Department", related="employee_id.department_id.name")
    job_position = fields.Char(string="Job Position", related="employee_id.job_title", readonly=True)

    state = fields.Selection(selection=state_selection, string='State', default='draft')
    loan_reason = fields.Text(string='Reason')

    date_loan_start = fields.Date(string='EMI Start Date', required=True)
    date_approve = fields.Date(string='Approve Date')
    date_apply = fields.Date(readonly=True)
    num_of_installment = fields.Integer(string='No. of Installment', required=True)
    amount_loan = fields.Float(string='Requested Loan Amount', required=True)
    amount_approved = fields.Float(string='Amount Approved', readonly=True, required=True)
    per_emi_amount = fields.Float(compute='_compute_per_emi_amount')
    emi_amount = fields.Float(string='EMI Amount', readonly=True)
    # loan_policy = fields.Many2one(comodel_name="loan.policy", required=True, string="Loan Policy", domain="[('state', '=', 'confirm')]")
    emi_detail_line = fields.One2many('loan.emi.details', 'loan_request_id', string="EMI Details")
    remaining_loan_amount = fields.Float(string="Remaining Loan Amount", compute="_compute_remaining_loan_amount", store=True)

    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id.id)

    user_group_check = fields.Boolean(string="check field", compute='_compute_get_user')
    readonly_installment = fields.Boolean(string="Installment Readonly", compute='_compute_get_user')

    @api.depends('state')
    def _compute_get_user(self):
        if self.env.user.has_group('loan.group_loan_officer') or self.env.user.has_group('loan.group_loan_manager'):
            self.user_group_check = True
            self.readonly_installment = False
        else:
            self.user_group_check = False
            if self.state == 'apply':
                self.readonly_installment = True
            else:
                self.readonly_installment = False

    @api.depends('emi_detail_line.loan_status')
    def _compute_remaining_loan_amount(self):
        # approved loan amount - (calculated paid amount from installment lines)
        for rec in self:
            rec.remaining_loan_amount = 0
            for record in rec.emi_detail_line:
                if not record.loan_status:
                    rec.remaining_loan_amount += record.emi_amount

    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.employee_id.name))
        return result

    @api.depends('amount_loan', 'num_of_installment')
    def _compute_per_emi_amount(self):
        for record in self:
            if record.num_of_installment:
                record.per_emi_amount = record.amount_loan / record.num_of_installment
            else:
                record.per_emi_amount = 0

    @api.constrains('date_loan_start')
    def _check_date(self):
        if self.date_loan_start and self.date_loan_start < fields.Date.today():
            raise ValidationError("'Start Date' must not before today.")

    @api.constrains('amount_loan', 'num_of_installment')
    def validate_loan(self):
        if self.amount_loan > self.loan_policy.maximum_loan_amount:
            raise ValidationError("Requested amount can't be greater than %s." % self.loan_policy.maximum_loan_amount)
        if self.amount_loan < self.loan_policy.minimum_loan_amount:
            raise ValidationError("Requested amount can't be less than %s." % self.loan_policy.minimum_loan_amount)
        if self.num_of_installment > self.loan_policy.max_installment_number:
            raise ValidationError("Number of Installment can't be greater than %s." % self.loan_policy.max_installment_number)
        if self.num_of_installment < self.loan_policy.min_installment_number:
            raise ValidationError("Number of Installment can't be lower than %s." % self.loan_policy.min_installment_number)
        return True

    def action_apply(self):
        self.ensure_one()
        loan_accept = self.validate_loan()
        self.amount_approved = self.amount_loan
        if loan_accept:
            return self.write({'state': 'apply', 'date_apply': fields.Date.today()})
        else:
            self.write({'state': 'cancel'})
            return UserError("You are not eligible for loan yet.")

    def action_confirm(self):
        self.ensure_one()
        current_user = self.env.user
        super_user = current_user.id == SUPERUSER_ID
        manager_approval_access = current_user.has_group('loan.group_loan_manager')

        if not (manager_approval_access or super_user):
            raise UserError("Only Manager/Admin can approve Loan.")
        self.validate_loan()

        line_value = []
        day_emi_month = int(datetime.strftime(self.date_loan_start, "%d"))
        emi_amount = self.amount_approved / self.num_of_installment
        if day_emi_month == 1:
            next_month = self.date_loan_start
        else:
            next_month = self.date_loan_start - relativedelta(days=day_emi_month - 1)
        for installment in range(self.num_of_installment):
            str_date = format_date(self.env, fields.Date.to_string(next_month), date_format='MMMM yyyy')
            line_value.append((0, 0, {
                'str_date_emi': str_date,
                'date_emi': next_month,
                'emi_amount': emi_amount,
            }))
            next_month = next_month + relativedelta(months=1)
        self.update({'emi_detail_line': line_value})

        return self.write({'date_approve': fields.Date.today(), 'state': 'confirm', 'emi_amount': emi_amount})

    def action_cancel(self):
        self.ensure_one()
        current_user = self.env.user
        super_user = current_user.id == SUPERUSER_ID
        officer_approval_access = current_user.has_group('loan.group_loan_officer')
        manager_approval_access = current_user.has_group('loan.group_loan_manager')
        if not (officer_approval_access or manager_approval_access or super_user):
            raise UserError("Only loan Manager/HR/Admin can Reject this request.")
        else:
            return self.write({'state': 'cancel'})

    def action_done(self):
        if self.remaining_loan_amount != 0:
            raise ValidationError("Employee must pay %s BDT to close the loan." % self.remaining_loan_amount)

    def unlink(self):
        for record in self:
            if record.state == "approve":
                raise UserError(message="You can't delete any approved request.", title="Permission Error")
            super(LoanRequest, record).unlink()

    def write(self, vals):
        return super(LoanRequest, self).write(vals)


class EmiDetails(models.Model):
    _name = 'loan.emi.details'

    emi_amount = fields.Float(string='EMI Amount', force_save=1)
    str_date_emi = fields.Char(string="EMI Date")
    date_emi = fields.Date(string="EMI Date")
    skip_month = fields.Boolean(default=False, string='Skip Month')
    loan_status = fields.Boolean(default=False, string='Loan Status')

    loan_request_id = fields.Many2one('loan.request')

    @api.onchange('skip_month')
    def skip_month_change(self):
        if self.skip_month and (self.loan_status == False):
            if self.loan_request_id.loan_policy.skip_policy == 'new_installment_line':
                date_emi = self.loan_request_id.emi_detail_line[-1].date_emi + relativedelta(months=1)
                str_date = format_date(self.env, fields.Date.to_string(date_emi), date_format='MMMM yyyy')
                line = [(0, 0, {
                    'str_date_emi': str_date,
                    'date_emi': date_emi,
                    'emi_amount': self.emi_amount,
                })]
                self._origin.loan_request_id.update({'emi_detail_line': line})
                self.update({'skip_month': True, 'emi_amount': 0})
            else:
                active_line = len(list(filter(lambda x: x.skip_month == False and x.loan_status == False, self.loan_request_id.emi_detail_line)))
                updated_emi_amount = self.loan_request_id.remaining_loan_amount / active_line
                self.update({'skip_month': True, 'emi_amount': 0})
                for rec in self.loan_request_id.emi_detail_line:
                    if rec.loan_status == False and rec.skip_month == False:
                        rec._origin.update({'emi_amount': updated_emi_amount})
