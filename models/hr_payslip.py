from odoo import api, Command, fields, models, _
from odoo.exceptions import UserError, ValidationError


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    is_refund_salary = fields.Boolean(string="Refund Type", default=False)

    def refund_sheet(self):
        if not self.is_refund_salary:
            self.is_refund_salary = True

        return super(HrPayslip, self).refund_sheet()

    @api.depends('employee_id')
    def action_payslip_done(self):

        # Update Loan Status
        emi_lines = self.env['loan.emi.details'].sudo()
        employee_loan_inst_lines = emi_lines.search([('loan_request_id.employee_id', '=', self.employee_id.id),
                                                             ('date_emi', '=', self.date_from),
                                                             ('skip_month', '=', False)])
        for line in employee_loan_inst_lines:
            if self.is_refund_salary:
                line.update({'loan_status': False})
            else:
                line.update({'loan_status': True})

        return super(HrPayslip, self).action_payslip_done()

    def action_payslip_cancel(self):
        emi_lines = self.env['loan.emi.details'].sudo()
        employee_loan_inst_lines = emi_lines.search([('loan_request_id.employee_id', '=', self.employee_id.id),
                                                     ('date_emi', '=', self.date_from),
                                                     ('skip_month', '=', False)])
        for line in employee_loan_inst_lines:
            if not self.is_refund_salary:
                line.update({'loan_status': False})
            else:
                line.update({'loan_status': True})

        return super(HrPayslip, self).action_payslip_cancel()
