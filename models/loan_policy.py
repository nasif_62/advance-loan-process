from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError


class LoanPolicy(models.Model):
    _name = 'loan.policy'
    _description = "Loan Policy Details"

    skip_policy_option = [('new_installment_line', 'New Installment Line'), ('recalculate_remaining_amount', 'Recalculate Remaining Amount')]

    name = fields.Char(string='Loan Policy Name', required=True)
    active = fields.Boolean(default=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed')
    ], string='Status', default='draft')

    skip_policy = fields.Selection(selection=skip_policy_option, string="Skip Policy", required=True)
    minimum_loan_amount = fields.Float(string="Minimum Loan Amount", required=True)
    maximum_loan_amount = fields.Float(string="Maximum Loan Amount", required=True)
    max_installment_number = fields.Integer(string="Max No. of Installment", required=True)
    min_installment_number = fields.Integer(string="Min No. of Installment", required=True)

    _sql_constraints = [('no_zero_negative_installment', 'CHECK(max_installment_number > 0)', 'Max no of installment must be greater than Zero.')]

    def is_exists(self):
        return self.search_count([('state', '=', 'confirm'), ('id', '!=', self.id)]) > 0

    def action_confirm(self):
        for record in self:
            if record.is_exists():
                raise UserError("Another Policy is in confirm state, please set that configuration to draft first!")
            record.state = 'confirm'

    def action_draft(self):
        for record in self:
            record.state = 'draft'

